import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NoteService } from '../../../services/note.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from '../../../models/category.model';
import { Note } from '../../../models/note.model';
import { CategoryService } from '../../../services/category.service';
import { NotificationService } from '../../../services/notification.service';
import { NgForm } from '@angular/forms';
import { NoteDto } from '../../../dtos/note-dto';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../../../services/authentication.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.css']
})
export class EditNoteComponent implements OnInit {
  @ViewChild(NgForm) form: NgForm;
  @Input() note: Note = new Note(0, '', 0, '', '', '', '');

  private loggedOutSubscription: Subscription;
  private action = 'New';
  private noteLoaded = false;
  private category: Category;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private categoryService: CategoryService,
    private noteService: NoteService,
  ) { }

  ngOnInit() {
    const categoryId = +this.route.snapshot.paramMap.get('category_id');
    const noteId = +this.route.snapshot.paramMap.get('note_id');

    this.categoryService.get(categoryId)
      .subscribe(
        (result: Category) => {
          this.category = result;
        },
        () => this.notificationService.error('Error while trying to retrieve notes for category!')
      );

    if (noteId) {
      this.action = 'Edit';
      this.noteService.get(noteId)
        .subscribe(
          (result: Note) => {
            this.note = result;
            this.noteLoaded = true;
          },
          () => this.notificationService.error('Error while trying to retrieve selected note!')
        );
    }

    this.loggedOutSubscription = this.authenticationService.loggedIn$
      .subscribe(
        (p: boolean) => {
          if (!p) {
            this.router.navigate(['/home']);
          }
        }
      );
  }

  public save(form: NgForm) {
    if (!form.valid) {
      this.notificationService.error('Error in note data!');
      return;
    }
    let observable: Observable<Note>;
    if (this.note.id) {
      observable = this.noteService.save(this.note.id, NoteDto.fromModel(this.note));
    } else {
      const noteDto = NoteDto.fromModel(this.note);
      noteDto.categoryId = this.category.id;
      observable = this.noteService.create(noteDto);
    }
    observable.subscribe(
      () => {
        this.notificationService.success('Note saved succesfully!');
        this.back();
      },
      () => this.notificationService.error('Error while saving the note!')
    );

  }

  private back() {
    this.router.navigate(['categories/', this.category.id]);
  }
}
