import { FormGroup } from '@angular/forms';
export class PasswordMatchValidation {

    static MatchPassword(g: FormGroup) {
        const passMatch = g.get('password').value === g.get('passwordConfirm').value;
        if (!passMatch) {
            g.get('passwordConfirm').setErrors({match: false});
        }
        return passMatch ? null : { 'mismatch': true };
    }
}
