import { FormGroup } from '@angular/forms/src/model';

export class UserRegistrationDto {
    public static fromFormGroup(form: FormGroup) {
        return new UserRegistrationDto(
            form.value.firstName,
            form.value.lastName,
            form.value.username,
            form.value.email,
            form.value.password,
        );
    }

    constructor(
        public firstName,
        public lastName,
        public username,
        public email,
        public password
    ) { }
}
