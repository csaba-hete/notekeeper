package hu.elte.alkfejl.notekeeper.enumerated;

import lombok.Getter;

import java.util.Objects;

public enum Visibility {
    PUBLIC("PUBLIC"), PRIVATE("PRIVATE");

    @Getter
    private String value;

    Visibility(String value){
        this.value = value;
    }

    public static Visibility parse(String value){
        Visibility visibility = null;
        for (Visibility item : Visibility.values()) {
            if (Objects.equals(item.getValue(), value)) {
                visibility = item;
                break;
            }
        }
        return visibility;
    }
}
