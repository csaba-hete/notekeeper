import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class NotificationService {
  private duration = 3000;

  constructor(
    private snackBar: MatSnackBar
  ) { }

  public success(message: string): void {
    this.openSnackBar(message, 'notification-success-msg');
  }

  public error(message: string): void {
    this.openSnackBar(message, 'notification-error-msg');
  }

  public warning(message: string): void {
    this.openSnackBar(message, 'notification-warning-msg');
  }

  private openSnackBar(message: string, type: string): void {
    const classes: string[] = ['notification-msg'];
    this.snackBar.open(message, null, {
      duration: this.duration,
      extraClasses: classes.concat([type]),
      horizontalPosition: 'right'
    });
  }
}
