export class Note {
  constructor(
    public id: number,
    public ownerName: string,
    public categoryId: number,
    public title: string,
    public content: string,
    public createdAt: string,
    public updatedAt: string,
  ) { }
}
