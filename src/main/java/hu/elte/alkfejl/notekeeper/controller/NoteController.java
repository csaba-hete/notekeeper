package hu.elte.alkfejl.notekeeper.controller;

import hu.elte.alkfejl.notekeeper.annotation.Role;
import hu.elte.alkfejl.notekeeper.dto.NoteCreateDto;
import hu.elte.alkfejl.notekeeper.entity.Note;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.service.CategoryService;
import hu.elte.alkfejl.notekeeper.service.NoteService;
import hu.elte.alkfejl.notekeeper.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum.USER;

@RestController
@RequestMapping("/api/note")
public class NoteController {

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private NoteService noteService;

    @GetMapping("{id}")
    public ResponseEntity<Note> show(@PathVariable long id){
        try {
            return ResponseEntity.ok(noteService.getById(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("")
    @Role({USER})
    public ResponseEntity<Note> store(@RequestBody NoteCreateDto note){
        try {
            return ResponseEntity.ok(noteService.create(note));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("{id}")
    @Role({USER})
    public ResponseEntity<Note> update(@PathVariable long id, @RequestBody NoteCreateDto note){
        try {
            return ResponseEntity.ok(noteService.update(id, note));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("{id}")
    @Role({USER})
    public ResponseEntity<Boolean> delete(@PathVariable long id){
        try {
            return ResponseEntity.ok(noteService.delete(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }
}