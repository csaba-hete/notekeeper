package hu.elte.alkfejl.notekeeper.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@DynamicInsert
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USERS")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User extends EntityWithTimeStamps{

    public User(String firstName, String lastName, String username, String email, String password) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    @Column(name="first_name", nullable=false, columnDefinition = "VARCHAR(255)")
    @Size(min = 2, max = 255)
    private String firstName;

    @Column(name="last_name", nullable=false, columnDefinition = "VARCHAR(255)")
    @Size(min = 2, max = 255)
    private String lastName;

    @Column(nullable=false, unique = true, columnDefinition = "VARCHAR(255)")
    @Size(min = 4, max = 255)
    private String username;

    @Column(nullable=false, unique = true, columnDefinition = "VARCHAR(255)")
    private String email;

    @Column(nullable=false, columnDefinition = "VARCHAR(100)")
    private String password;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "ROLE_USER",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
    )
    private List<Role> roles;

    @OneToMany(mappedBy = "user", targetEntity = Category.class, fetch = FetchType.LAZY)
    private List<Category> categories;

    @OneToMany(mappedBy = "owner", targetEntity = Note.class, fetch = FetchType.LAZY)
    private List<Note> notes;

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public List<Role.RoleEnum> getRoles() {
        return null == this.roles ? Collections.emptyList(): this.roles.stream().map(r-> Role.RoleEnum.parse(r.getName())).collect(Collectors.toList());
    }

    @JsonProperty
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public List<Category> getCategories() {
        return categories;
    }

    @JsonProperty
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @JsonIgnore
    public List<Note> getNotes() {
        return notes;
    }

    @JsonProperty
    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
}
