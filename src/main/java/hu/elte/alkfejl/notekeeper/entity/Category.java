package hu.elte.alkfejl.notekeeper.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import hu.elte.alkfejl.notekeeper.enumerated.Visibility;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@DynamicInsert
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CATEGORIES")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Category extends EntityWithTimeStamps {

    @Column(nullable=false, unique = true, columnDefinition = "VARCHAR(255)")
    @Size(min = 4, max = 255)
    private String name;

    @Column(nullable=false, columnDefinition = "VARCHAR(255)")
    @Size(max = 255)
    private String description;

    @Column(nullable=false, columnDefinition = "VARCHAR(10) DEFAULT 'PUBLIC'")
    private String visibility;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name="user_id", nullable = false, referencedColumnName="id")
    private User user;

    @OneToMany(mappedBy = "category", targetEntity = Note.class, fetch = FetchType.LAZY)
    private List<Note> notes;

    public Category(String name, String description, String visibility) {
        super();
        this.name = name;
        this.description = description;
        this.visibility = visibility;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Visibility getVisibility () {
        return Visibility.parse(this.visibility);
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getOwnerName(){
        return this.user.getUsername();
    }

    @JsonIgnore
    public User getUser() {
        return user;
    }

    @JsonProperty
    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty
    public void setVisibiity(Visibility visibility) {
        this.visibility = visibility.getValue();
    }

    @JsonIgnore
    public List<Note> getNotes() {
        return notes;
    }

    @JsonProperty
    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
}
