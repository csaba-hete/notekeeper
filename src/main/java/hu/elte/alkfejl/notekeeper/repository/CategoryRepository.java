package hu.elte.alkfejl.notekeeper.repository;

import hu.elte.alkfejl.notekeeper.entity.Category;
import hu.elte.alkfejl.notekeeper.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long>{

    List<Category> getAllByVisibility(String visibility);
    List<Category> getAllByVisibilityAndUser(String visibility, User user);
    List<Category> getAllByVisibilityOrUser(String visibility, User user);
    List<Category> getAllByUser(User user);
    Optional<Category> getById(long id);
    Optional<Category> getByIdAndVisibility(long id, String value);
    Optional<Category> getByIdAndUser(long id, User user);
}
