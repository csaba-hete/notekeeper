import {Component, OnInit, ViewChild} from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { User } from '../../../models/user.model';
import { UserService } from '../../../services/user.service';
import { NotificationService } from '../../../services/notification.service';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../../../services/authentication.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public loadInProgress = false;
  public displayedColumns = ['name', 'username', 'email', 'created_at'];
  public dataSource: UserDataSource;

  private selectedUser: User;

  @ViewChild('sidebar') sidebar;

  constructor(
    public authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private userService: UserService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.refreshUsers();
    this.authenticationService.loggedIn$.subscribe(
      (p: boolean) => {
        this.refreshUsers();
      }
    );
  }

  public refreshUsers() {
    this.dataSource = new UserDataSource(this.userService, this.notificationService);
  }

}

export class UserDataSource extends DataSource<User> {
  public resultsLength = 0;
  public isLoadingResults: boolean;

  constructor(
    private userService: UserService,
    private notificationService: NotificationService,
  ) {
    super();
  }

  connect(): Observable<User[]> {
    return Observable.merge([])
      .startWith(() => this.isLoadingResults = true)
      .switchMap(() => {
        return this.userService.getUsers();
      })
      .map((data: User[]) => {
        this.isLoadingResults = false;
        this.resultsLength = data.length;
        return data;
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.notificationService.error('Error during refreshing users!');
        return Observable.of([]);
      });
  }

  disconnect(): void { }
}
