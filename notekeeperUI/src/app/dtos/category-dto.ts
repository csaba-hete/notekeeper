import { Category } from '../models/category.model';
import { FormGroup } from '@angular/forms';

export class CategoryDto {
  public static fromFormGroup(form: FormGroup) {
    return new CategoryDto(
      form.value.name,
      form.value.visibility,
      form.value.description,
    );
  }

  public static fromModel(category: Category) {
    return new CategoryDto(
      category.name,
      category.visibility,
      category.description,
    );
  }

  constructor(
    public name: string,
    public visibility: string,
    public description: string,
  ) { }
}
