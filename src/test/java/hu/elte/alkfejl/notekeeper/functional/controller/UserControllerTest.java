package hu.elte.alkfejl.notekeeper.functional.controller;

import hu.elte.alkfejl.notekeeper.dto.UserLoginDto;
import hu.elte.alkfejl.notekeeper.dto.UserRegistrationDto;
import hu.elte.alkfejl.notekeeper.service.AuthenticationService;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.jdbc.JdbcTestUtils.countRowsInTable;


@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AuthenticationService authenticationService;

    private HttpHeaders headers;

    @Before
    public void setUp() {
        headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, APPLICATION_JSON.toString());
    }

    @Test
    public void getUser_notLoggedIn() throws Exception {
        // Act
        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user",
                HttpMethod.GET, null, String.class);

        //Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void get() throws Exception {
        // Arrange
        UserLoginDto admin = new UserLoginDto("admin@notekeeper.hu", "123456");
        HttpEntity<UserLoginDto> entity = new HttpEntity<>(admin, headers);
        restTemplate.exchange(
                "/api/user/login",
                HttpMethod.POST, entity, String.class);
        assertNotNull(authenticationService.getLoggedInUser());

        // Act
        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user",
                HttpMethod.GET, null, String.class);

        //Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(response.getBody())
                .contains("id")
                .contains("firstName")
                .contains("lastName")
                .contains("email")
                .doesNotContain("password");}


    @Test
    public void getUserById() throws Exception {
        // Act
        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user/1",
                HttpMethod.GET, null, String.class);

        //Assert
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    public void register() {
        // Arrange
        UserRegistrationDto user = new UserRegistrationDto("Test", "User", "testuser", "testuser@notekeeper.hu", "asdf1234");
        HttpEntity<UserRegistrationDto> entity = new HttpEntity<>(user, headers);

        final String USERS_TABLE = "USERS";
        final String ROLES_TABLE = "ROLES";
        final String ROLE_USER_TABLE = "ROLE_USER";

        final int user_count = countRowsInTable(new JdbcTemplate(dataSource), USERS_TABLE);
        final int role_count = countRowsInTable(new JdbcTemplate(dataSource),ROLES_TABLE);
        final int role_user_count = countRowsInTable(new JdbcTemplate(dataSource),ROLE_USER_TABLE);

        // Act
        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user/register",
                HttpMethod.POST, entity, String.class);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(user_count + 1, countRowsInTable(new JdbcTemplate(dataSource), USERS_TABLE));
        assertEquals(role_count, countRowsInTable(new JdbcTemplate(dataSource),ROLES_TABLE));
        assertEquals(role_user_count + 1, countRowsInTable(new JdbcTemplate(dataSource),ROLE_USER_TABLE));
    }

    @Test
    public void login() {
        // Arrange
        UserLoginDto admin = new UserLoginDto("admin@notekeeper.hu", "123456");
        HttpEntity<UserLoginDto> entity = new HttpEntity<>(admin, headers);

        // Act
        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user/login",
                HttpMethod.POST, entity, String.class);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(response.getBody())
                .contains("id")
                .contains("firstName")
                .contains("lastName")
                .contains("email")
                .doesNotContain("password");
    }

    @Test
    public void login_shouldFail() {
        UserLoginDto nonExistingUser = new UserLoginDto("user@notekeeper.hu", "none");
        HttpEntity<UserLoginDto> entity = new HttpEntity<>(nonExistingUser, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user/login",
                HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void logout() throws Exception {
        // Arrange
        UserLoginDto admin = new UserLoginDto("admin@notekeeper.hu", "123456");
        HttpEntity<UserLoginDto> entity = new HttpEntity<>(admin, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                "/api/user/login",
                HttpMethod.POST, entity, String.class);
        assertNotNull(authenticationService.getLoggedInUser());

        // Act
        response = restTemplate.exchange(
                "/api/user/logout",
                HttpMethod.POST, null, String.class);

        //Assert
        assertNull(authenticationService.getLoggedInUser());
    }

}