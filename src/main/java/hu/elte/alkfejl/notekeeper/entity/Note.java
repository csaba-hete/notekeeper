package hu.elte.alkfejl.notekeeper.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@DynamicInsert
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "NOTES")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Note extends EntityWithTimeStamps {

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "owner_id", nullable = false, referencedColumnName = "id")
    private User owner;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Category.class)
    @JoinColumn(name = "category_id", nullable = false, referencedColumnName = "id")
    private Category category;

    @Column(nullable = false, columnDefinition = "VARCHAR2(255)")
    @Size(max = 255)
    private String title;

    @Column(columnDefinition = "TEXT")
    private String content;

    public Note(String title, String content) {
        super();
        this.title = title;
        this.content = content;
    }

    @JsonIgnore
    public User getOwner() {
        return owner;
    }

    @JsonProperty
    public void setOwner(User owner) {
        this.owner = owner;
    }

    @JsonIgnore
    public Category getCategory() {
        return category;
    }

    @JsonProperty
    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOwnerName(){
        return this.owner.getUsername();
    }
}
