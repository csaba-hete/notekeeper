import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { NoteService } from '../../../services/note.service';
import { Category } from '../../../models/category.model';
import { Note } from '../../../models/note.model';
import { NotificationService } from '../../../services/notification.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AuthenticationService } from '../../../services/authentication.service';
import { Notification } from 'rxjs/Notification';
import { NoteDto } from '../../../dtos/note-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ViewNoteDialogComponent } from '../../dialogs/view-note-dialog/view-note-dialog.component';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {
  public loadInProgress = false;
  public displayedColumns = ['title', 'ownerName', 'created_at', 'updated_at', 'actions'];
  public dataSource: NoteDataSource;

  private category: Category;
  constructor(
    public authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private categoryService: CategoryService,
    private noteService: NoteService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.dataSource = new NoteDataSource(this.category, this.noteService, this.notificationService);

    const id = +this.route.snapshot.paramMap.get('category_id');
    this.categoryService.get(id)
      .subscribe(
        (result: Category) => {
          this.category = result;
          this.refreshNotes();
        },
        () => this.notificationService.error('Error while trying to retrieve notes for category!')
      );
  }

  public refreshNotes() {
    this.dataSource = new NoteDataSource(this.category, this.noteService, this.notificationService);
  }

  public new(): void {
    if (this.authenticationService.isAuthenticated) {
      this.router.navigate(['/categories/', this.category.id, 'add-note']);
    } else {
      this.unauthorized();
    }
  }

  public edit(note: Note): void {
    if (this.authenticationService.isAuthenticated) {
      this.router.navigate(['/categories/', this.category.id, 'edit-note', note.id]);
    } else {
      this.unauthorized();
    }
  }

  public delete(note: Note): void {
    if (this.authenticationService.isAuthenticated) {
      this.noteService.delete(note.id)
      .subscribe(
        () => {
          this.notificationService.success('Deleted successfully!');
          this.refreshNotes();
        },
        () => this.notificationService.error('Error during deletion!')
      );
    } else {
      this.unauthorized();
    }
  }

  public show(note: Note): void {
    const dialogRef = this.dialog.open(ViewNoteDialogComponent, {
      data: {
        category: this.category,
        note: note
      }
    });
  }

  private unauthorized() {
    this.notificationService.error('You must log in for this operation!');
  }
}

export class NoteDataSource extends DataSource<Note> {
  public resultsLength = 0;
  public isLoadingResults = false;

  constructor(
    private category: Category,
    private noteService: NoteService,
    private notificationService: NotificationService,
  ) {
    super();
  }

  connect(): Observable<Note[]> {
    if (this.category) {
      return Observable.merge([])
      .startWith(() => this.isLoadingResults = true)
      .switchMap(() => {
        return this.noteService.getNotesByCategoryId(this.category.id);
      })
      .map((data: Note[]) => {
        this.isLoadingResults = false;
        this.resultsLength = data.length;

        return data;
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.notificationService.error('Error during refreshing notes!');
        return Observable.of([]);
      });
    }
    return Observable.merge([]);
  }

  disconnect(): void { }
}
