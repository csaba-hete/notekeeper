export class User {

  constructor(public id: number,
              public firstName: string,
              public lastName: string,
              public username: string,
              public email: string,
              public createdAt,
              public updatedAt) {
  }

  public static getFullName(user: User): string {
    return `${user.firstName} ${user.lastName}`
    .replace(/\s{2,}/g, ' ')
    .trim();
  }
}
