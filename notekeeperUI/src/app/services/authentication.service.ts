import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { User } from '../models/user.model';
import { UserLoginDto } from '../dtos/user-login-dto';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map, tap } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable()
export class AuthenticationService {
  private _currentUser: User = null;
  private loggedIn = false;

  public loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);

  constructor(
    private userService: UserService,
    private http: Http
  ) {
    const currentUser = localStorage.getItem('currentUser');
    if (currentUser) {
      this.userService.getCurrentUser()
      .subscribe(
        (user: User) => {
          if (!user) {
            localStorage.removeItem('currentUser');
          } else {
            this.setLoggedIn(JSON.parse(currentUser));
          }
        },
        () => {
          localStorage.removeItem('currentUser');
        }
      );
    }
  }

  public authenticate(userLoginDto: UserLoginDto): Observable<User> {
    return this.http.post('api/user/login', userLoginDto)
      .map((response: Response) => response.json() as User || null)
      .pipe(
        tap((user: User) => {
          this.setLoggedIn(user);
          localStorage.setItem('currentUser', JSON.stringify(user));
        })
      )
      .catch((err: Response) => Observable.throw(err));
  }

  public logout(): Observable<any>  {
    return this.http.post('api/user/logout', {})
    .map((response: Response) => this.setLoggedIn(null))
    .pipe(
      tap(() => localStorage.removeItem('currentUser'))
    )
    .catch((err: Response) => Observable.throw(err));
  }

  public setLoggedIn(user?: User) {
    this._currentUser = user;
    this.loggedIn = !!user;
    this.loggedIn$.next(this.loggedIn);
  }

  get currentUser(): User {
    return this._currentUser;
  }

  get isAuthenticated(): boolean {
    return !!this.currentUser;
  }
}
