package hu.elte.alkfejl.notekeeper.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Role {
    hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum[] value() default {
            hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum.GUEST
    };
}