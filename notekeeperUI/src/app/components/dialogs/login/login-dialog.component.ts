import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AuthenticationService } from '../../../services/authentication.service';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../models/user.model';
import { UserLoginDto } from '../../../dtos/user-login-dto';
import { NotificationService } from '../../../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {
  private loginForm: FormGroup;
  private submitted: boolean;

  public buttonDisabled = false;

  constructor(
    private dialogRef: MatDialogRef<LoginDialogComponent>,
    private authService: AuthenticationService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.email,
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.minLength(6),
        Validators.required
      ])
    });
  }

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }
  get formError() { return this.submitted && !this.loginForm.valid; }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public onSubmit(): void {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.authService.authenticate(UserLoginDto.fromFormGroup(this.loginForm))
        .subscribe(
        (user: User) => this.dialogRef.close(user),
        (err: Response) => {
          this.buttonDisabled = false;
          switch (err.status) {
            case 400:
              this.notificationService.error('Invalid credentials!');
              break;
            default:
              this.notificationService.error('Server error, try again later!');
          }
        },
        () => this.buttonDisabled = false
        );
    }
  }

  public onNoClick(): void {
    this.dialogRef.close(null);
  }
}
