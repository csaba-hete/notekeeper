import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../models/user.model';
import { LoginDialogComponent } from '../dialogs/login/login-dialog.component';
import { NotificationService } from '../../services/notification.service';
import { RegisterDialogComponent } from '../dialogs/register-dialog/register-dialog.component';
import { AuthGuard } from '../../guards/auth.guard';
import { Subscription } from 'rxjs/Subscription';
import { UnauthorizedRouteData } from '../../guards/unauthorized-route-data';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit, OnDestroy {
    unauthorizedSubscription: Subscription;

  constructor(
    public authGuard: AuthGuard,
    public authenticationService: AuthenticationService,
    private router: Router,
    private notificationService: NotificationService,
    private dialog: MatDialog
  ) { }

  public login() {
    const dialogRef = this.dialog.open(LoginDialogComponent);

    dialogRef.afterClosed()
      .subscribe((result: User) => {
        if (result) {
          this.notificationService.success('Signed in succesfully!');
        }
      });
  }

  public logout() {
    this.authenticationService.logout()
      .subscribe(
      (response: Response) => {
        this.notificationService.success('Signed out succesfuly!');
      },
      (err: Response) => {
        switch (err.status) {
          default:
            this.notificationService.error('Error during logout!');
        }
      }
      );
  }

  public register() {
    const dialogRef = this.dialog.open(RegisterDialogComponent);

    dialogRef.afterClosed()
      .subscribe((result: User) => {
        if (result) {
          this.notificationService.success('Succesful registration!');
        }
      });
  }

  ngOnInit() {
    this.unauthorizedSubscription = this.authGuard.unauthorized$.asObservable()
      .subscribe(
      (data: UnauthorizedRouteData) => {
        const dialogRef = this.dialog.open(LoginDialogComponent);

        dialogRef.afterClosed()
          .subscribe((result: User) => {
            if (result) {
              this.notificationService.success('Signed in succesfully!');
              this.router.navigate([data.url]);
            }
          });
      }
      );
  }

  ngOnDestroy() {
    this.unauthorizedSubscription.unsubscribe();
  }
}
