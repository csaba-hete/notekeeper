import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { SignUpService } from '../../../services/sign-up.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../models/user.model';
import { PasswordMatchValidation } from '../../../validators/password-match-validation';
import { UserRegistrationDto } from '../../../dtos/user-registration-dto';
import { UserLoginDto } from '../../../dtos/user-login-dto';
import { NotificationService } from '../../../services/notification.service';


@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.css']
})
export class RegisterDialogComponent implements OnInit {
  private registerForm: FormGroup;
  private submitted: boolean;

  public passwordsAreTheSame = true;
  public buttonDisabled = false;

  constructor(
    private dialogRef: MatDialogRef<RegisterDialogComponent>,
    private authService: AuthenticationService,
    private signUpService: SignUpService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      firstName: new FormControl('', [
        Validators.minLength(2),
        Validators.required
      ]),
      lastName: new FormControl('', [
        Validators.minLength(2),
        Validators.required
      ]),
      username: new FormControl('', [
        Validators.minLength(4),
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.email,
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.minLength(6),
        Validators.required
      ]),
      passwordConfirm: new FormControl('', [
        Validators.required,
      ]),
    }, PasswordMatchValidation.MatchPassword);
  }

  get firstName() { return this.registerForm.get('firstName'); }
  get lastName() { return this.registerForm.get('lastName'); }
  get username() { return this.registerForm.get('username'); }
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get passwordConfirm() { return this.registerForm.get('passwordConfirm'); }
  get formError() { return this.submitted && !this.registerForm.valid; }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public onSubmit(): void {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.signUpService.register(UserRegistrationDto.fromFormGroup(this.registerForm))
        .subscribe(
        () => {
          this.notificationService.success('Succesful registration!');
          this.authService.authenticate(UserLoginDto.fromFormGroup(this.registerForm))
          .subscribe(
          (user: User) => this.dialogRef.close(user),
          this.handleLoginError
          );
        },
        this.handleRegistrationError,
        () => this.buttonDisabled = false
        );
    }
  }

  public onNoClick(): void {
    this.dialogRef.close(null);
  }

  private handleRegistrationError(err: Response): void {
    this.buttonDisabled = false;
    switch (err.status) {
      case 400:
        this.notificationService.error('Invalid registration data provided!');
        break;
      default:
        this.notificationService.error('Server error, try again later!');
    }
  }

  private handleLoginError(err: Response): void {
    this.buttonDisabled = false;
    switch (err.status) {
      default:
        this.notificationService.error('Error during signing in!');
    }
  }
}
