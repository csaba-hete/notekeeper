package hu.elte.alkfejl.notekeeper.controller;

import hu.elte.alkfejl.notekeeper.annotation.Role;
import hu.elte.alkfejl.notekeeper.dto.UserLoginDto;
import hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum;
import hu.elte.alkfejl.notekeeper.entity.User;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.exception.InvalidCredentialsException;
import hu.elte.alkfejl.notekeeper.service.AuthenticationService;
import hu.elte.alkfejl.notekeeper.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;

import javax.xml.ws.Response;

import java.util.List;
import java.util.Set;

import static hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum.ADMIN;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public ResponseEntity<User> get() {
        if (authenticationService.isLoggedIn()) {
            return ResponseEntity.ok(authenticationService.getLoggedInUser());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("{id}")
    @Role({ADMIN})
    public ResponseEntity<User> getUserById(@PathVariable long id) {
        try {
            return ResponseEntity.ok(userService.getById(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/list")
    //@Role({ADMIN})
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping("/register")
    public ResponseEntity<Boolean> register(@RequestBody User user) {
        try {
            userService.register(user);
            return ResponseEntity.ok(true);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody UserLoginDto user) {
        try {
            authenticationService.login(user);
            return ResponseEntity.ok(authenticationService.getLoggedInUser());
        } catch (InvalidCredentialsException | EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<User> logout() {
        authenticationService.logout();
        return ResponseEntity.ok().build();
    }
}