import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CategoryDto } from '../dtos/category-dto';
import { Note } from '../models/note.model';
import { NoteDto } from '../dtos/note-dto';

@Injectable()
export class NoteService {

  constructor(private http: Http) {
  }

  public getNotesByCategoryId(categoryId: number): Observable<Note[]> {
    return this.http.get(`api/category/${categoryId}/notes`)
      .map(this.extractData)
      .catch((err: Response) => Observable.throw(err));
  }

  public get(id: number): Observable<Note> {
    return this.http.get(`api/note/${id}`)
      .map((response: Response) => response.json() as Note || null)
      .catch((err: Response) => Observable.throw(err));
  }

  public create(note: NoteDto): Observable<Note> {
    return this.http.post(`api/note`, note)
      .map(this.extractData)
      .catch((err: Response) => Observable.throw(err));
  }

  public save(id: number, note: NoteDto): Observable<Note> {
    return this.http.put(`api/note/${id}`, note)
      .map(this.extractData)
      .catch((err: Response) => Observable.throw(err));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`api/note/${id}`)
      .catch((err: Response) => Observable.throw(err));
  }

  extractData(response: Response): Note[] {
    return response.json() as Note[] || [];
  }

}
