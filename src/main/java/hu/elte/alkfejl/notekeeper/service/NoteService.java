package hu.elte.alkfejl.notekeeper.service;

import hu.elte.alkfejl.notekeeper.dto.NoteCreateDto;
import hu.elte.alkfejl.notekeeper.entity.Category;
import hu.elte.alkfejl.notekeeper.entity.Note;
import hu.elte.alkfejl.notekeeper.enumerated.Visibility;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.repository.NoteRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.List;

@Service
@SessionScope
@Data
public class NoteService {
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private NoteRepository noteRepository;

    public Note getById(long id) throws EntityNotFoundException {
        Note n = noteRepository.getById(id).orElseThrow(EntityNotFoundException::new);
        Category category = n.getCategory();
        if (category.getVisibility().equals(Visibility.PUBLIC) ||
                category.getUser().equals(authenticationService.getLoggedInUser()))
            return n;

        return null;
    }

    List<Note> getAllByCategory(Category c) throws EntityNotFoundException {
        if (null == c || (c.getVisibility().equals(Visibility.PRIVATE) &&
                !c.getUser().equals(authenticationService.getLoggedInUser()))) {
            throw new EntityNotFoundException();
        }
        return noteRepository.getAllByCategory(c);
    }

    public Note create(NoteCreateDto note) throws EntityNotFoundException {
        return noteRepository.save(this.createFromDto(note));
    }

    public boolean delete(long id) throws EntityNotFoundException {
        Note n = this.getById(id);

        noteRepository.delete(n);
        return true;
    }

    public Note update(long id, NoteCreateDto dto) throws EntityNotFoundException {
        Note n = this.getById(id);

        n.setTitle(dto.getTitle());
        n.setContent(dto.getContent());
        return noteRepository.save(n);
    }

    private Note createFromDto(NoteCreateDto dto) throws EntityNotFoundException {
        Note n = new Note(dto.getTitle(), dto.getContent());
        n.setOwner(authenticationService.getLoggedInUser());
        n.setCategory(categoryService.getById(dto.getCategoryId()));
        return n;
    }
}
