import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Category } from '../../../models/category.model';
import { NotificationService } from '../../../services/notification.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AuthenticationService } from '../../../services/authentication.service';
import { MatDialog } from '@angular/material';
import { EditCategoryDialogComponent } from '../../dialogs/edit-category-dialog/edit-category-dialog.component';
import { CategoryDto } from '../../../dtos/category-dto';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})

export class CategoryListComponent implements OnInit {
  public loadInProgress = false;
  public displayedColumns = ['name', 'visibility', 'ownerName', 'created_at', 'updated_at', 'actions'];
  public dataSource: CategoryDataSource;

  private selectedCategory: Category;

  @ViewChild('sidebar') sidebar;

  constructor(
    public authenticationService: AuthenticationService,
    private notificationService: NotificationService,
    private categoryService: CategoryService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.refreshCategories();
    this.authenticationService.loggedIn$.subscribe(
      (p: boolean) => {
        this.refreshCategories();
      }
    );
  }

  public refreshCategories() {
    this.dataSource = new CategoryDataSource(this.categoryService, this.notificationService);
  }

  public showInfo(category: Category): void {
    this.selectedCategory = category;
    this.sidebar.open();
  }

  public new(): void {
    if (this.authenticationService.isAuthenticated) {
      const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
        data: {
          id: 0,
          model: undefined
        }
      });
      dialogRef.afterClosed()
        .subscribe((result: Category) => {
          if (result) {
            this.notificationService.success('Category created succesfully!');
            this.refreshCategories();
          }
        });
    } else {
      this.unauthorized();
    }
  }

  public edit(category: Category): void {
    if (this.authenticationService.isAuthenticated) {
      const dialogRef = this.dialog.open(EditCategoryDialogComponent, {
        data: {
          id: category.id,
          model: CategoryDto.fromModel(category)
        }
      });
      dialogRef.afterClosed()
        .subscribe((result: Category) => {
          if (result) {
            this.notificationService.success('Category saved succesfully!');
            this.refreshCategories();
          }
        });
    } else {
      this.unauthorized();
    }
  }

  public delete(category: Category): void {
    if (this.authenticationService.isAuthenticated) {
      this.categoryService.delete(category.id)
        .subscribe(
          () => {
            this.notificationService.success('Deleted successfully!');
            this.refreshCategories();
          },
          () => this.notificationService.error('Error during deletion!')
        );
    } else {
      this.unauthorized();
    }
  }

  private unauthorized() {
    this.notificationService.error('You must log in for this operation!');
  }
}

export class CategoryDataSource extends DataSource<Category> {
  public resultsLength = 0;
  public isLoadingResults: boolean;

  constructor(
    private categoryService: CategoryService,
    private notificationService: NotificationService,
  ) {
    super();
  }

  connect(): Observable<Category[]> {
    return Observable.merge([])
      .startWith(() => this.isLoadingResults = true)
      .switchMap(() => {
        return this.categoryService.getCategories();
      })
      .map((data: Category[]) => {
        this.isLoadingResults = false;
        this.resultsLength = data.length;
        return data;
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.notificationService.error('Error during refreshing categories!');
        return Observable.of([]);
      });
  }

  disconnect(): void { }
}
