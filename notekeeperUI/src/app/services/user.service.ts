import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user.model';

@Injectable()
export class UserService {

  constructor(private http: Http) {}

  public getCurrentUser(): Observable<User> {
    return this.http.get('api/user')
      .map((response: Response) => response.json() as User || null)
      .catch((err: Response) => Observable.throw(err));
  }

  public getUsers(): Observable<User[]> {
    return this.http.get('api/user/list')
      .map(this.extractData)
      .catch((err: Response) => Observable.throw(err));
  }



  extractData(response: Response): User[] {
    return response.json() as User[] || [];
  }
}
