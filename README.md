# NoteKeeper

Beadandó feladat az ELTE `Alkalmazások fejlesztése` kurzusához.  
[Dokumentáció](https://gitlab.com/csaba-hete/notekeeper/wikis/documentation)

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Angular](https://angular.io/)
* [Spring](https://spring.io/)
* [Project Lombok](https://projectlombok.org/)

## Authors

* **Hete Csaba** - [gitlab](https://gitlab.com/csaba-hete), [github](https://github.com/csabahete)
* **Gajdács Ádám** -