package hu.elte.alkfejl.notekeeper.repository;

import hu.elte.alkfejl.notekeeper.entity.Role;
import hu.elte.alkfejl.notekeeper.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    Optional<Role> findByName(String name);
}