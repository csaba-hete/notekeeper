package hu.elte.alkfejl.notekeeper.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import static java.util.Objects.hash;

@Data
@MappedSuperclass
class Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Override
    public boolean equals(Object object) {
        if (null == object || !(object.getClass().equals(this.getClass()))) {
            return false;
        }

        final Identifiable other = (Identifiable) object;
        return 0 != this.getId() && this.getId() == (other.getId());
    }

    @Override
    public int hashCode() {
        return 0 == getId() ? 0 : hash(getId());
    }
}
