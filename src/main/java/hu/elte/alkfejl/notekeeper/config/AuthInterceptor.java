package hu.elte.alkfejl.notekeeper.config;

import hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum;
import hu.elte.alkfejl.notekeeper.entity.User;
import hu.elte.alkfejl.notekeeper.service.AuthenticationService;
import hu.elte.alkfejl.notekeeper.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Set<RoleEnum> routeRoles = getRoles((HandlerMethod) handler);
        // when there are no restrictions, we let the user through
        if (routeRoles.isEmpty() || routeRoles.contains(RoleEnum.GUEST)) {
            return true;
        }
        // check role
        if (authenticationService.isLoggedIn() && hasRole(routeRoles)) {
            return true;
        }

        response.setStatus(401);
        return false;
    }

    private boolean hasRole(Set<RoleEnum> routeRoles) {
        User user = authenticationService.getLoggedInUser();
        if (null == user) {
            return false;
        }
        routeRoles.retainAll(new HashSet<>(user.getRoles()));
        return !routeRoles.isEmpty();
    }

    private Set<RoleEnum> getRoles(HandlerMethod handler) {
        hu.elte.alkfejl.notekeeper.annotation.Role role = handler.getMethodAnnotation(hu.elte.alkfejl.notekeeper.annotation.Role.class);
        return role == null ? Collections.emptySet() : new TreeSet<>(Arrays.asList(role.value()));
    }
}