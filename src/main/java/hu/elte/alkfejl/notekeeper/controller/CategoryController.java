package hu.elte.alkfejl.notekeeper.controller;

import hu.elte.alkfejl.notekeeper.annotation.Role;
import hu.elte.alkfejl.notekeeper.dto.CategoryCreateDto;
import hu.elte.alkfejl.notekeeper.entity.Category;
import hu.elte.alkfejl.notekeeper.entity.Note;
import hu.elte.alkfejl.notekeeper.enumerated.Visibility;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.exception.ValidationFailedException;
import hu.elte.alkfejl.notekeeper.service.AuthenticationService;
import hu.elte.alkfejl.notekeeper.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static hu.elte.alkfejl.notekeeper.entity.Role.RoleEnum.USER;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("")
    public ResponseEntity<List<Category>> index() {
        if (!authenticationService.isLoggedIn()) {
            return ResponseEntity.ok(categoryService.getAllByVisibility(Visibility.PUBLIC));
        }

        return ResponseEntity.ok(categoryService.getPublicAndOwnedByUser(authenticationService.getLoggedInUser()));
    }

    @GetMapping("{id}")
    public ResponseEntity<Category> show(@PathVariable long id) {
        try {
            return ResponseEntity.ok(categoryService.getById(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("")
    @Role({USER})
    public ResponseEntity<Category> store(@RequestBody CategoryCreateDto category) {
        try {
            return ResponseEntity.ok(categoryService.create(category));
        } catch (ValidationFailedException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("{id}")
    @Role({USER})
    public ResponseEntity<Category> update(@PathVariable long id, @RequestBody CategoryCreateDto category) {
        try {
            return ResponseEntity.ok(categoryService.update(id, category));
        } catch (ValidationFailedException | EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("{id}")
    @Role({USER})
    public ResponseEntity<Boolean> delete(@PathVariable long id) {
        try {
            return ResponseEntity.ok(categoryService.delete(id));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("{id}/notes")
    public ResponseEntity<List<Note>> index(@PathVariable long id) throws EntityNotFoundException {
        return ResponseEntity.ok(categoryService.getAllNotesForCategory(id));
    }
}