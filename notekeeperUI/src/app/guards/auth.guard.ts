import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Subject } from 'rxjs/Subject';
import { UnauthorizedRouteData } from './unauthorized-route-data';

@Injectable()
export class AuthGuard implements CanActivate {
  public unauthorized$ = new Subject<UnauthorizedRouteData>();

  constructor(
    private router: Router,
    public authenticationService: AuthenticationService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('currentUser') || this.authenticationService.isAuthenticated) {
      return true;
    }

    this.unauthorized$.next(new UnauthorizedRouteData(state.url));
    return false;
  }
}
