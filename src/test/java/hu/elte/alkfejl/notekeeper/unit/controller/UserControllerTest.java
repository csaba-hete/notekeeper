package hu.elte.alkfejl.notekeeper.unit.controller;

import hu.elte.alkfejl.notekeeper.config.AuthInterceptor;
import hu.elte.alkfejl.notekeeper.controller.UserController;
import hu.elte.alkfejl.notekeeper.dto.UserLoginDto;
import hu.elte.alkfejl.notekeeper.entity.User;
import hu.elte.alkfejl.notekeeper.exception.InvalidCredentialsException;
import hu.elte.alkfejl.notekeeper.service.AuthenticationService;
import hu.elte.alkfejl.notekeeper.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private AuthInterceptor authInterceptor;

    @Before
    public void setUp() throws Exception {
        Mockito.when(authInterceptor.preHandle(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(true);
    }

    @Test
    public void getUser() throws Exception {
        // Arrange
        Mockito.when(authenticationService.isLoggedIn()).thenReturn(true);
        Mockito.when(authenticationService.getLoggedInUser()).thenReturn(new User());

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/user")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertThat(result.getResponse().getContentAsString())
                .contains("id")
                .contains("firstName")
                .contains("lastName")
                .contains("email")
                .doesNotContain("password");
    }

    @Test
    public void getUser_shouldFail() throws Exception {
        // Arrange
        Mockito.when(authenticationService.isLoggedIn()).thenReturn(false);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/user")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
    }

    @Test
    public void register() throws Exception {
        // Arrange
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"firstName\":\"Test\", \"lastName\":\"User\",\"username\":\"testuser\",\"email\":\"testuser@notekeeper.hu\",\"password\":\"asdf1234\"}")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        Mockito.verify(userService, Mockito.times(1)).register(Mockito.any(User.class));
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    public void login() throws Exception {
        // Arrange
        UserLoginDto user = new UserLoginDto("user@notekeeper.hu", "nope");

        Mockito.when(authenticationService.login(user)).thenReturn(new User());
        Mockito.when(authenticationService.getLoggedInUser()).thenReturn(
                new User("", "", "user", "", "")
        );

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"user\", \"password\":\"nope\"}")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertThat(result.getResponse().getContentAsString())
                .contains("id")
                .contains("firstName")
                .contains("lastName")
                .contains("email")
                .doesNotContain("password");
    }

    @Test
    public void login_shouldFail() throws Exception {
        // Arrange
        UserLoginDto user = new UserLoginDto("user@notekeeper.hu", "nope");

        Mockito.when(authenticationService.login(user)).thenThrow(InvalidCredentialsException.class);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/user/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"user\", \"password\":\"nope\"}")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
    }

    @Test
    public void logout() throws Exception {
        // Arrange
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/user/logout")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        Mockito.verify(authenticationService, Mockito.times(1)).logout();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

}