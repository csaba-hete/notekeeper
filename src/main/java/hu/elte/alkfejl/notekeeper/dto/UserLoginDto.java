package hu.elte.alkfejl.notekeeper.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginDto {
    private String email;
    private String password;

    public String getEmail() { return email; }

    public String getPassword() { return password; }
}
