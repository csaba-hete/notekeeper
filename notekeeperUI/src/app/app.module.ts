import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { appRoutes } from './app.routing';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
// import { CdkTableModule } from '@angular/cdk/table';

// 3rd parties
import { CKEditorModule } from 'ng2-ckeditor';

// guards
import { AuthGuard } from './guards/auth.guard';
// services
import { AuthenticationService } from './services/authentication.service';
import { SignUpService } from './services/sign-up.service';
import { NotificationService } from './services/notification.service';
import { CategoryService } from './services/category.service';
import { NoteService } from './services/note.service';
// components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { LoginDialogComponent } from './components/dialogs/login/login-dialog.component';
import { RegisterDialogComponent } from './components/dialogs/register-dialog/register-dialog.component';
import { CategoryListComponent } from './components/category/category-list/category-list.component';
import { EditCategoryDialogComponent } from './components/dialogs/edit-category-dialog/edit-category-dialog.component';
import { NoteListComponent } from './components/note/note-list/note-list.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { UserService } from './services/user.service';
import { EditNoteComponent } from './components/note/edit-note/edit-note.component';
import { ViewNoteDialogComponent } from './components/dialogs/view-note-dialog/view-note-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    LoginDialogComponent,
    RegisterDialogComponent,
    CategoryListComponent,
    EditCategoryDialogComponent,
    NoteListComponent,
    UserListComponent,
    EditNoteComponent,
    ViewNoteDialogComponent,
  ],
  imports: [
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    // material design modules
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatMenuModule,
    // 3rd parties
    CKEditorModule
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    SignUpService,
    NotificationService,
    CategoryService,
    NoteService,
    UserService,
  ],
  entryComponents: [
    LoginDialogComponent,
    RegisterDialogComponent,
    EditCategoryDialogComponent,
    ViewNoteDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
