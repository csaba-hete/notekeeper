package hu.elte.alkfejl.notekeeper.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Date;

@Data
@MappedSuperclass
abstract class EntityWithTimeStamps extends Identifiable{

    @Column(name = "created_at", nullable = false, columnDefinition = "DATE DEFAULT SYSDATE")
    private Date createdAt = new Date(new java.util.Date().getTime());

    @Column(name = "updated_at", nullable = false, columnDefinition = "DATE DEFAULT SYSDATE")
    private Date updatedAt = new Date(new java.util.Date().getTime());
}
