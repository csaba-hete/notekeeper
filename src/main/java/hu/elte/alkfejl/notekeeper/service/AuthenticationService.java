package hu.elte.alkfejl.notekeeper.service;

import hu.elte.alkfejl.notekeeper.dto.UserLoginDto;
import hu.elte.alkfejl.notekeeper.entity.User;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.exception.InvalidCredentialsException;
import hu.elte.alkfejl.notekeeper.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Optional;

@SessionScope
@Service
public class AuthenticationService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordService passwordService;

    private User user;

    public User login(UserLoginDto user) throws EntityNotFoundException, InvalidCredentialsException {
        Optional<User> optional = userRepository.findByEmail(user.getEmail());
        User u = optional.orElseThrow(EntityNotFoundException::new);
        if (!passwordService.matches(user.getPassword(), u.getPassword())){
            throw new InvalidCredentialsException();
        }
        return this.user = u;
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        this.user = null;
    }

    public User getLoggedInUser() {
        return this.user;
    }
}