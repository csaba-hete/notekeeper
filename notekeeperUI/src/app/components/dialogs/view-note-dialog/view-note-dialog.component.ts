import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NoteService } from '../../../services/note.service';
import { NotificationService } from '../../../services/notification.service';
import { Category } from '../../../models/category.model';
import { Note } from '../../../models/note.model';

@Component({
  selector: 'app-view-note-dialog',
  templateUrl: './view-note-dialog.component.html',
  styleUrls: ['./view-note-dialog.component.css']
})
export class ViewNoteDialogComponent implements OnInit {

  private category: Category;
  private notes: any;
  private selectedNote: Note;

  constructor(
    private dialogRef: MatDialogRef<ViewNoteDialogComponent>,
    private noteService: NoteService,
    private notificationService: NotificationService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.category = this.data.category || null;
    this.selectedNote = this.data.note || null;

    if (this.category) {
      this.noteService.getNotesByCategoryId(this.category.id)
      .subscribe(
        (notes: Note[]) => {
          this.notes = this.makeIterator(notes);
        },
        () => this.notificationService.error('Error during displaying the note!')
      );
    }
  }

  public next(): void {
    this.selectedNote = this.notes.next();
  }

  public previous(): void {
    this.selectedNote = this.notes.previous();
  }

  public close(): void {
    this.dialogRef.close(null);
  }

  private makeIterator(array: Array<Note>): any {
    let currentIndex = 0;

    return {
       next: function() {
           return (currentIndex < array.length) ? array[currentIndex++] : array[currentIndex = 0];
       },
       previous: function() {
           return currentIndex > 0 ? array[--currentIndex] : array[currentIndex = array.length - 1];
       },
       current: function() {
           return array[currentIndex] || null;
       }
    };
}
}
