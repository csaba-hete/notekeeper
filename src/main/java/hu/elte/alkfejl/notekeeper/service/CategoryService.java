package hu.elte.alkfejl.notekeeper.service;

import hu.elte.alkfejl.notekeeper.dto.CategoryCreateDto;
import hu.elte.alkfejl.notekeeper.entity.Category;
import hu.elte.alkfejl.notekeeper.entity.Note;
import hu.elte.alkfejl.notekeeper.entity.User;
import hu.elte.alkfejl.notekeeper.enumerated.Visibility;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.exception.ValidationFailedException;
import hu.elte.alkfejl.notekeeper.repository.CategoryRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Data
public class CategoryService {
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> getPublicAndOwnedByUser(User user) {
        if (null == user) {
            throw new IllegalArgumentException();
        }

        List<Category> categories = getAllByVisibility(Visibility.PUBLIC);
        categories.addAll(categoryRepository.getAllByVisibilityAndUser(Visibility.PRIVATE.getValue(), user));
        return categories;
    }

    public List<Category> getAllByVisibility(Visibility v) {
        if (null == v) {
            throw new IllegalArgumentException();
        }

        return categoryRepository.getAllByVisibility(v.getValue());
    }

    public Category getById(long id) throws EntityNotFoundException {
        Optional<Category> c;
        if (authenticationService.isLoggedIn()){
            User loggedInUser = authenticationService.getLoggedInUser();
            c = categoryRepository.getByIdAndUser(id, loggedInUser);
        } else {
            c = categoryRepository.getByIdAndVisibility(id, Visibility.PUBLIC.getValue());
        }
        return c.orElseThrow(EntityNotFoundException::new);
    }

    public Category create(CategoryCreateDto category) throws ValidationFailedException {
        return categoryRepository.save(this.createFromDto(category));
    }

    public boolean delete(long id) throws EntityNotFoundException {
        Category c = this.getById(id);
        categoryRepository.delete(c);
        return true;
    }

    public Category update(long id, CategoryCreateDto dto) throws ValidationFailedException, EntityNotFoundException {
        Category c = this.getById(id);
        Visibility v = getVisibility(dto);

        c.setName(dto.getName());
        c.setDescription(dto.getDescription());
        c.setVisibiity(v);
        return categoryRepository.save(c);
    }

    public List<Note> getAllNotesForCategory(long id) throws EntityNotFoundException {
        Category c = categoryRepository.getById(id).orElseThrow(EntityNotFoundException::new);
        if (c.getVisibility().equals(Visibility.PUBLIC) ||
                authenticationService.isLoggedIn() && c.getUser().equals(authenticationService.getLoggedInUser())) {
            return noteService.getAllByCategory(c);
        }
        return Collections.emptyList();
    }

    private Visibility getVisibility(CategoryCreateDto dto) throws ValidationFailedException {
        Visibility v = Visibility.parse(dto.getVisibility());
        if (null == v) {
            throw new ValidationFailedException();
        }
        return v;
    }

    private Category createFromDto(CategoryCreateDto dto) throws ValidationFailedException {
        Visibility v = getVisibility(dto);
        Category category = new Category(dto.getName(), dto.getDescription(), v.getValue());
        category.setUser(authenticationService.getLoggedInUser());
        return category;
    }
}
