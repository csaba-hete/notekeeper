package hu.elte.alkfejl.notekeeper.service;

import hu.elte.alkfejl.notekeeper.entity.Role;
import hu.elte.alkfejl.notekeeper.entity.User;
import hu.elte.alkfejl.notekeeper.exception.EntityNotFoundException;
import hu.elte.alkfejl.notekeeper.repository.RoleRepository;
import hu.elte.alkfejl.notekeeper.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
@Data
public class UserService {
    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordService passwordService;

    public void register(User user) throws EntityNotFoundException {
        user.setPassword(passwordService.hash(user.getPassword()));
        Role role = roleRepository.findByName("USER").orElseThrow(EntityNotFoundException::new);
        user.setRoles(Arrays.asList(role));
        userRepository.save(user);
    }

    public User getById(long id) throws EntityNotFoundException {
        return this.userRepository.getById(id).orElseThrow(EntityNotFoundException::new);
    }

    public List<User> getUsers() {
        List<User> users = userRepository.findAll();
        return users;
    }
}