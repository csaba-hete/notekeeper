package hu.elte.alkfejl.notekeeper.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
    private BCryptPasswordEncoder hasher;

    public PasswordService() {
        super();
        this.hasher = new BCryptPasswordEncoder();
    }

    public String hash(String password) {
        return hasher.encode(password);
    }

    public boolean matches(String pw, String hash) {
        return hasher.matches(pw, hash);
    }
}
