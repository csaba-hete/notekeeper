import { Note } from '../models/note.model';
import { FormGroup } from '@angular/forms';

export class NoteDto {
  public static fromFormGroup(categoryId: number, form: FormGroup) {
    return new NoteDto(
      categoryId,
      form.value.title,
      form.value.content,
    );
  }

  public static fromModel(note: Note) {
    return new NoteDto(
      note.categoryId,
      note.title,
      note.content,
    );
  }

  constructor(
    public categoryId: number,
    public title: string,
    public content: string,
  ) { }
}
