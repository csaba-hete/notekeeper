import { Component, OnInit, Inject, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../services/notification.service';
import { CategoryDto } from '../../../dtos/category-dto';
import { CategoryService } from '../../../services/category.service';
import { Observable } from 'rxjs/Observable';
import { Category } from '../../../models/category.model';

@Component({
  selector: 'app-edit-category-dialog',
  templateUrl: './edit-category-dialog.component.html',
  styleUrls: ['./edit-category-dialog.component.css']
})
export class EditCategoryDialogComponent implements OnInit {
  private editCategoryForm: FormGroup;
  private submitted: boolean;

  public buttonDisabled = false;
  public visibilities = [
    {
      label: 'Public',
      value: 'PUBLIC'
    },
    {
      label: 'Private',
      value: 'PRIVATE'
    },
  ];

  constructor(
    private dialogRef: MatDialogRef<EditCategoryDialogComponent>,
    private categoryService: CategoryService,
    private notificationService: NotificationService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit() {
    // TODO: This
    this.editCategoryForm = new FormGroup({
      name: new FormControl(this.data.model ? this.data.model.name : '', [
        Validators.required
      ]),
      visibility: new FormControl(this.data.model ? this.data.model.visibility : 'PUBLIC', [
        Validators.required
      ]),
      description: new FormControl(this.data.model ? this.data.model.description : '', [
        Validators.maxLength(255),
      ])
    });
  }

  get name() { return this.editCategoryForm.get('name'); }
  get description() { return this.editCategoryForm.get('description'); }
  get formError() { return this.submitted && !this.editCategoryForm.valid; }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public onSubmit(): void {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      let category: Observable<Category> = null;
      if (this.data.id) {
        category = this.categoryService.save(this.data.id, CategoryDto.fromFormGroup(this.editCategoryForm));
      } else {
        category = this.categoryService.create(CategoryDto.fromFormGroup(this.editCategoryForm));
      }
      category.subscribe(
        (result: Category) => {
          this.buttonDisabled = false;
          this.dialogRef.close(result);
        },
        () => {
          this.buttonDisabled = false;
          this.notificationService.error('There was an error during the operation!');
        }
      );
    }
  }

  public onNoClick(): void {
    this.dialogRef.close(null);
  }
}
