package hu.elte.alkfejl.notekeeper.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryCreateDto {
    private String name;
    private String visibility;
    private String description;

    //public String getName() {
    //    return name;
    //}
//
    //public String getDescription() {
    //    return description;
    //}
//
    //public String getVisibility() {
    //    return visibility;
    //}
}
