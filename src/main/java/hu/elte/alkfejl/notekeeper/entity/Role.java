package hu.elte.alkfejl.notekeeper.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@DynamicInsert
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ROLES")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Role extends EntityWithTimeStamps {

    public enum RoleEnum {
        GUEST("GUEST"), USER("USER"), ADMIN("ADMIN"), DEVELOPER("DEVELOPER");

        @Getter
        private final String value;

        RoleEnum(String value) {
            this.value = value;
        }

        public static RoleEnum parse(String value){
            RoleEnum role = null;
            for (RoleEnum item : RoleEnum.values()) {
                if (Objects.equals(item.getValue(), value)) {
                    role = item;
                    break;
                }
            }
            return role;
        }
    }

    public Role(String name) {
        super();
        this.name = name;
    }

    @Column(nullable = false, unique = true, columnDefinition = "VARCHAR(255)")
    private String name;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<User> users;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Set<User> getUsers() {
        return users;
    }

    @JsonProperty
    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
