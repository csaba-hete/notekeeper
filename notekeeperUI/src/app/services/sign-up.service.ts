import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms/src/model';

import { Http, Response } from '@angular/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs/Observable';
import { UserRegistrationDto } from '../dtos/user-registration-dto';

@Injectable()
export class SignUpService {

  constructor(
    private http: Http
  ) { }

  public register(userRegistrationDto: UserRegistrationDto): Observable<any> {
    return this.http.post('api/user/register', userRegistrationDto)
      .catch((err: Response) => Observable.throw(err));
  }
}
