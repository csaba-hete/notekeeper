import { FormGroup } from '@angular/forms/src/model';

export class UserLoginDto {
    public static fromFormGroup(form: FormGroup) {
        return new UserLoginDto(
            form.value.email,
            form.value.password,
        );
    }

    constructor(
        public email,
        public password
    ) { }
}
