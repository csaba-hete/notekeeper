package hu.elte.alkfejl.notekeeper.unit.controller;

import hu.elte.alkfejl.notekeeper.config.AuthInterceptor;
import hu.elte.alkfejl.notekeeper.controller.CategoryController;
import hu.elte.alkfejl.notekeeper.dto.CategoryCreateDto;
import hu.elte.alkfejl.notekeeper.service.AuthenticationService;
import hu.elte.alkfejl.notekeeper.service.CategoryService;
import hu.elte.alkfejl.notekeeper.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CategoryController.class, secure = false)
public class CategoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    @MockBean
    private CategoryService categoryService;

    @MockBean
    private AuthInterceptor authInterceptor;

    @MockBean
    private AuthenticationService authenticationService;

    @Before
    public void setUp() throws Exception {
        Mockito.when(authInterceptor.preHandle(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(true);
    }

    @Test
    public void index() throws Exception {
        // Arrange
        Mockito.when(authenticationService.isLoggedIn()).thenReturn(false);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/category")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        Mockito.verify(categoryService, Mockito.times(1)).getPublicAndOwnedByUser(Mockito.any());
    }

    @Test
    public void store() throws Exception {
        // Arrange
        Mockito.doReturn(true).when(authenticationService).isLoggedIn();
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/api/category")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Testcategory\", \"description\":\"Just a test category\",\"visibility\":\"PUBLIC\"}")
                .accept(MediaType.APPLICATION_JSON);

        // Act
        MvcResult result = mvc.perform(requestBuilder).andReturn();

        // Assert
        Mockito.verify(categoryService, Mockito.times(1)).create(Mockito.any(CategoryCreateDto.class));
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

}