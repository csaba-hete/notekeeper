export class Category {
  constructor(
    public id: number,
    public name: string,
    public description: string,
    public visibility: string,
    public ownerName: string,
    public createdAt: string,
    public updatedAt: string,
  ) { }
}
