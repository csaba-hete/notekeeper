package hu.elte.alkfejl.notekeeper.repository;

import hu.elte.alkfejl.notekeeper.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmail(String email);
    Optional<User> getById(long id);
    List<User> findAll();
}