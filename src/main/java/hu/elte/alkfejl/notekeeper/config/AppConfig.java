package hu.elte.alkfejl.notekeeper.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

@Configuration
public class AppConfig {
    @Bean
    public HandlerInterceptor handlerInterceptor() {
        return new AuthInterceptor();
    }
}