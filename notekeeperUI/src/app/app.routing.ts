import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { CategoryListComponent } from './components/category/category-list/category-list.component';
import { NoteListComponent } from './components/note/note-list/note-list.component';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { EditNoteComponent } from './components/note/edit-note/edit-note.component';

export const appRoutes: Routes = [
  { path: 'home', component: HomeComponent}, // canActivate: [AuthGuard]
  { path: 'categories', component: CategoryListComponent},
  { path: 'categories/:category_id', component: NoteListComponent},
  { path: 'categories/:category_id/add-note', component: EditNoteComponent, canActivate: [AuthGuard]},
  { path: 'categories/:category_id/edit-note/:note_id', component: EditNoteComponent, canActivate: [AuthGuard]},
  { path: 'users', component: UserListComponent, canActivate: [AuthGuard]},

  // otherwise redirect to home
  { path: '**', redirectTo: 'home' }
];
