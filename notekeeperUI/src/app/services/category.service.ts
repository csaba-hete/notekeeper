import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Category } from '../models/category.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CategoryDto } from '../dtos/category-dto';

@Injectable()
export class CategoryService {

  constructor(private http: Http) {
  }

  public getCategories(): Observable<Category[]> {
    return this.http.get('api/category')
      .map(this.extractData)
      .catch((err: Response) => Observable.throw(err));
  }

  public get(id: number): Observable<Category> {
    return this.http.get(`api/category/${id}`)
    .map(this.extractData)
    .catch((err: Response) => Observable.throw(err));
  }

  public create(category: CategoryDto): Observable<Category> {
    return this.http.post(`api/category`, category)
    .map(this.extractData)
    .catch((err: Response) => Observable.throw(err));
  }

  public save(id: number, category: CategoryDto): Observable<Category> {
    return this.http.put(`api/category/${id}`, category)
    .map(this.extractData)
    .catch((err: Response) => Observable.throw(err));
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`api/category/${id}`)
    .catch((err: Response) => Observable.throw(err));
  }

  extractData(response: Response): Category[] {
    return response.json() as Category[] || [];
  }
}
