package hu.elte.alkfejl.notekeeper.repository;

import hu.elte.alkfejl.notekeeper.entity.Category;
import hu.elte.alkfejl.notekeeper.entity.Note;
import hu.elte.alkfejl.notekeeper.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long>{

    List<Note> getAllByOwner(User owner);
    List<Note> getAllByCategory(Category category);
    Optional<Note> getByIdAndOwner(long id, User owner);
    Optional<Note> getById(long id);
}
